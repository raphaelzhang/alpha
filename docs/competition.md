* 复杂度估计：一千万次为一秒，例如算法复杂度若为$O(n^3)$，而$n <= 100$，则运算次数为$100^3=1,000,000$，小于一千万，可以在1秒之内运行完，若$n <= 1,000$，则$1,000^3=1,000,000,000$，大于一千万，则会超时

* `memset`是设置每个**字节**的值，因此对于一个int数组而言，要想通过`memset`将数组中的每个int都设置成1是不可能的，因为int是32位数，占4个字节，三个字节是0，一个字节是1，每个字节的值不一样

* 万能头文件：`#include <bits/stdc++.h>`，当然正常编程不能如此操作

* 竞赛普遍用全局变量和较短的变量名以节省时间，正常程序也不能如此操作

* 现场提供的样例数据非常少，需要自己写代码测试，一般可以使用`python`，下面是CSP/S 2020 第二轮T1用来生成输入数据的`python`程序（假设文件名为`gen_input.py`）

```python
n = 1000
print n
for i in xrange(n):
    print i+1
```

生成输入数据可运行`python gen_input.py > julian.in`，此命令是运行`python`程序，输入为`gen_input.py`，即调用python来运行`gen_input.py`脚本，并将输出的结果保存到`julian.in`文件中，其中`>`表示的是输出重定向，即不将程序的输出结果打印到屏幕上，而是输出到文件中。

* 下面是对应生成正确输出数据的`python`程序（假设文件名为`gen_ans.py`）

```python
def pr(y, m, d):
    if y < 0:
        print d, m, -y, "BC"
    else:
        print d, m, y

def is_leap(y):
    if y < 0:
        return (y+1) % 4 == 0
    elif y < 1583:
        return y % 4 == 0
    else:
        return (y % 400 == 0) or ((y % 4 == 0) and (y % 100) <> 0)

def next_day(y, m, d):
    months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if is_leap(y):
        months[1] = 29

    if m == 12 and d == 31:
        y, m, d = y+1, 1, 1
        if y == 0:
            y = 1
    elif months[m-1] == d:
        m, d = m+1, 1
    elif y == 1582 and m == 10 and d == 4:
        d = 15
    else:
        d = d + 1

    return (y, m, d)

n = 1000
y, m, d = -4713, 1, 2
for i in xrange(n):
    pr(y, m, d)
    y, m, d = next_day(y, m, d)
```

生成输出数据可运行`python gen_ans.py > ans.data`，此命令是运行`python`程序，输入为`gen_ans.py`，即调用python来运行`gen_ans.py`脚本，并将输出的结果保存到`ans.data`文件中，其中`>`表示的是输出重定向。

等待测试程序生成了`julian.out`，即可使用`diff`进行比较，即`diff --suppress-common-lines -y ans.data julian.out`

对于随机数据生成，则可以使用`python`里的`random`，如下面的程序打印了一百行数字，每个数字都是[1, 10000)内的随机数：

```python
import random

for i in xrange(100):
    print random.randint(1, 10000)

```

如果想测试程序运行时间，可以使用`time`命令，如`time ./my_prog`

除了生成测试数据与校准数据，使用`diff`对比之外，也可以开发对拍程序，也就是运行虽然比较慢，但是结果能保证正确的程序，例如开发冒泡排序作为对拍程序，来测试自己写的快速排序程序运行结果是否正确。

