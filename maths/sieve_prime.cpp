#include <iostream>

#define MAXN	(1<<27)
#define MAXP	(1<<24)

using namespace std;

char sieves[MAXN];
int primes[MAXP];
int total_primes;

int sieve(int n)
{
	for (int i = 2; i < n; i++) {
		if (!sieves[i])
			primes[total_primes++] = i;

		for (int j = 0; j < total_primes && primes[j]*i <= n; j++) {
			sieves[i * primes[j]] = 1;
			if (i % primes[j] == 0)
				break;
		}
	}
	return total_primes;
}

int main()
{
	int upper;
	cin >> upper;
	int count = sieve(upper);
	cout << "prime count: " << count << endl;
}
