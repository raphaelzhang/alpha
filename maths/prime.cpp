#include <iostream>

using namespace std;

bool is_prime(int n)
{
	if (n == 2)
		return true;

	if (n % 2 == 0)
		return false;

	int t = 3;	
	while (t * t <= n) {
		if (n % t == 0)
			return false;
		t += 2;
	}
	return true;
}

int main()
{
	int upper;
	cin >> upper;
	int primes = 0;
	for (int i = 2; i < upper; i++)
		if (is_prime(i))
			primes++;

	cout << "prime count: " << primes << endl;
}
