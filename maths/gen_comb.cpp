#include <iostream>

using namespace std;

void print_comb(int a[], int sel[], int n, int m, int cur_m, int pos)
{
	if (cur_m == m) {
		for (int i = 0; i < n; i++)
			if (sel[i])
				cout << a[i] << " ";
		cout << endl;
		return;
	}

	if (pos == n)
		return;

	sel[pos] = 1;
	print_comb(a, sel, n, m, cur_m+1, pos+1);
	sel[pos] = 0;
	print_comb(a, sel, n, m, cur_m, pos+1);
}

int main()
{
	int a[] = {2, 3, 5, 7, 11, 13};
	int sel[6] = {0};
	print_comb(a, sel, 6, 3, 0, 0);
}
