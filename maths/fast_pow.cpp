#include <cstdio>

using namespace std;

int fast_pow(int b, int p, int k)
{
	if (p == 0)
		return 1;

	if (p % 2 == 0) {
		int x = fast_pow(b, p >> 1, k);
		return (x * x) % k;
	} else {
		int x = fast_pow(b, (p-1) >> 1, k);
		int y = (x * x) % k;
		return (y * (b % k)) % k;
	}
}

// https://www.luogu.com.cn/problem/P1226
// echo "1025 1011011 10991099" | ./bin/fast_pow
int main()
{
	int b, p, k;
	scanf("%d %d %d", &b, &p, &k);

	printf("%d^%d mod %d=%d\n", b, p, k, fast_pow(b, p, k));
}
