#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

#define MAXINT 10000

struct bigint {
	vector<int> ints;

	bigint(int n) {
		while (n) {
			ints.push_back(n % MAXINT);
			n = (n - (n % MAXINT)) / MAXINT;
		}
	}

	bigint() {
		bigint(0);
	}

	bigint& operator=(const bigint& r) {
		ints.assign(r.ints.begin(), r.ints.end());
		return *this;
	}

	bool operator<(const bigint& b) {
		if (b.ints.size() != ints.size())
			return ints.size() < b.ints.size();

		for (int i = ints.size()-1; i >= 0; i--)
			if (ints[i] != b.ints[i])
				return ints[i] < b.ints[i];

		return false;
	}
};

void add_bi_bi(bigint& a, bigint& b, bigint& r)
{
	r.ints.clear();
	int carry = 0;
	for (int i = 0; i < a.ints.size() || i < b.ints.size(); i++) {
		int x = a.ints.size() <= i ? 0 : a.ints[i];
		int y = b.ints.size() <= i ? 0 : b.ints[i];
		int temp = x + y + carry;
		r.ints.push_back(temp % MAXINT);
		carry = temp / MAXINT;
	}

	if (carry != 0)
		r.ints.push_back(carry);
}

void mul_bi_i(bigint& a, int n, bigint& r)
{
	r.ints.clear();
	if (n == 0)
		return;

	int carry = 0;
	for (int i = 0; i < a.ints.size(); i++) {
		int t = a.ints[i] * n + carry;
		r.ints.push_back(t % MAXINT);
		carry = t / MAXINT;
	}
	if (carry != 0)
		r.ints.push_back(carry);
}

void mul_bi_bi(bigint& a, bigint& b, bigint& r)
{
	r.ints.clear();
	for (int i = 0; i < a.ints.size(); i++) {
		if (a.ints[i] == 0)
			continue;

		bigint t;
		mul_bi_i(b, a.ints[i], t);
		for (int j = 0; j < i; j++)
			t.ints.insert(t.ints.begin(), 0);
		bigint t2;
		add_bi_bi(t, r, t2);
		r = t2;
	}
}

void print_bi(bigint& bi)
{
	if (bi.ints.size() == 0) {
		cout << 0;
		return;
	}

	cout << bi.ints[bi.ints.size()-1];
	for (int i = bi.ints.size()-2; i >= 0; i--)
		printf("%04d", bi.ints[i]);
}

int main()
{
	vector<bigint> bis;
	int n;
	while (cin >> n) {
		bigint bi = n;
		bis.push_back(bi);
	}
	for (int i = 0; i < bis.size(); i++) {
		print_bi(bis[i]);
		cout << " ";
	}
	cout << endl;

	bigint a = 1, b = 1;
	for (int i = 0; i < bis.size()/2; i++) {
		bigint t;
		mul_bi_bi(a, bis[i], t);
		a = t;
	}
	print_bi(a);
	cout << endl;

	for (int i = bis.size()/2; i < bis.size(); i++) {
		bigint t;
		mul_bi_bi(b, bis[i], t);
		b = t;
	}
	print_bi(b);
	cout << endl;

	bigint c;
	add_bi_bi(a, b, c);
	print_bi(c);
	cout << endl;

	sort(bis.begin(), bis.end());

	for (int i = 0; i < bis.size(); i++) {
		print_bi(bis[i]);
		cout << " ";
	}
	cout << endl;
}
