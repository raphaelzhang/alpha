# alpha

#### Description

beginner's programs for learning algorithms

* basics:
	* sorting
		* [x] bubble
		* [x] insert
		* [x] shell
		* [x] quick
		* [x] merge
	* [x] binary search
	* [x] priority-queue
	* [x] Dijkstra (shortest path)
	* [x] Kruskal (minimum spanning tree)
	* [x] DFS
	* [x] BFS
	* [ ] topological sort
	* [ ] Euler path
	* [ ] dynamic programming
* maths:
	* [x] big integer
	* [x] simple prime number determination
	* [x] sieve for prime number determination
	* [x] fast power op
	* [x] GCD
	* [ ] Euler function
	* [ ] Fermat theorem
	* [x] combination
	* [x] permutation

union-find is included in Kruskal, so omitted
