#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;

int main()
{
	priority_queue<int> pq;
	int n;
	while (cin >> n)
		pq.push(n);

	while (!pq.empty()) {
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;
}
