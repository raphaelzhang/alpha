#include <iostream>
#include <cstring>

#define MAXN (1<<16)

using namespace std;

int a[MAXN];

int merge_temp[MAXN];

void merge(int x[], int s, int m, int e)
{
	int i = s, j = m, k = s;
	while (i < m && j < e) {
		merge_temp[k++] = x[i] < x[j] ? x[i++] : x[j++];
	}

	for (int h = j; h < e; h++, k++) {
		merge_temp[k] = x[h];
	}
	for (int h = i; h < m; h++, k++) {
		merge_temp[k] = x[h];
	}
	memcpy(&x[s], &merge_temp[s], sizeof(int)*(e - s));
}

void sort_data(int x[], int len)
{
	for (int w = 1; w < len; w <<= 1)
		for (int i = 0; i < len; i += 2*w) {
			int s = i, m = i+w > len ? len : i+w, e = i+2*w > len ? len : i+2*w;
			merge(x, s, m, e);
		}
}

bool is_sorted(int data[], int len)
{
	for (int i = 0; i < len-1; i++)
		if (data[i] > data[i+1])
			return false;

	return true;
}

int main()
{
	int n, total = 0;
	while (cin >> a[total])
		total++;

	sort_data(a, total);

	for (int i = 0; i < total && i < 100; i++)
		cout << a[i] << " ";
	cout << (total < 100 ? "" : "...") << endl;
	cout << (is_sorted(a, total) ? "sorted" : "not-sorted") << endl;
}
