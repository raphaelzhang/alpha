#include <iostream>
#include <algorithm>

#define MAXN 1000

using namespace std;

int a[MAXN];

int lower(int* data, int s, int e, int x)
{
	while (s < e) {
		int m = (s + e) / 2;
		if (data[m] < x)
			s = m+1;
		else
			e = m;
	}
	return s;
}

int main()
{
	int n, total = 0;
	while (cin >> a[total]) {
		total++;
	}

	sort(a, a+total);

	for (int i = 0; i < total; i++)
		cout << a[i] << " ";
	cout << endl;

	int x = 11;
	int pos = lower(a, 0, total, x);
	cout << x << " should be inserted at POS " << pos << " resulted data is:" << endl;

	for (int i = 0; i < pos; i++)
		cout << a[i] << " ";
	cout << x << " ";
	for (int i = pos; i < total; i++)
		cout << a[i] << " ";
	cout << endl;
}
