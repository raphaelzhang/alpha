#include <iostream>

#define MAXN (1<<16)

using namespace std;

int a[MAXN];

int select_pivot(int x[], int s, int e)
{
    int a = x[s], b = x[e-1], c = x[(s+e)/2];
    int t1 = min(min(a, b), c);
    int t2 = max(max(a, b), c);
    int t = a + b + c - t1 - t2;
    if (x[s] == t)
        return s;
    if (x[e-1] == t)
        return e-1;
    return (s+e)/2;
}

void quick_sort(int x[], int s, int e)
{
	if (s+1 >= e)
		return;

	int pos = select_pivot(x, s, e);
	int pivot = x[pos], m = s-1;
	for (int i = s; i < e; i++) {
		if (x[i] >= pivot)
			continue;

		m++;
		if (m < i) {
			int t = x[i];
			x[i] = x[m];
			x[m] = t;
		}
		if (m == pos)
			pos = i;
	}

	m++;
	if (pos != m) {
		int t = x[m];
		x[m] = x[pos];
		x[pos] = t;
	}

	int m1;
	for (m1 = m-1; m1 >= s; m1--)
		if (x[m1] < pivot)
			break;
	quick_sort(x, s, m1+1);

	int m2;
	for (m2 = m+1; m2 < e; m2++)
		if (x[m2] > pivot)
			break;
	quick_sort(x, m2, e);
}

void sort_data(int data[], int len)
{
	quick_sort(data, 0, len);
}

bool is_sorted(int data[], int len)
{
	for (int i = 0; i < len-1; i++)
		if (data[i] > data[i+1])
			return false;

	return true;
}

int main()
{
	int n, total = 0;
	while (cin >> a[total])
		total++;

	sort_data(a, total);

	for (int i = 0; i < total && i < 100; i++)
		cout << a[i] << " ";
	cout << (total < 100 ? "" : "...") << endl;
	cout << (is_sorted(a, total) ? "sorted" : "not-sorted") << endl;
}
