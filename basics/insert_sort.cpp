#include <iostream>

#define MAXN (1<<16)

using namespace std;

int a[MAXN];

void sort_data(int x[], int len)
{
	for (int i = 1; i < len; i++) {
		int j = i-1, last = x[i];
		while (j >= 0 && x[j] > last) {
			x[j+1] = x[j];
			j--;
		}
		x[j+1] = last;
	}
}

bool is_sorted(int data[], int len)
{
	for (int i = 0; i < len-1; i++)
		if (data[i] > data[i+1])
			return false;

	return true;
}

int main()
{
	int n, total = 0;
	while (cin >> a[total])
		total++;

	sort_data(a, total);

	for (int i = 0; i < total && i < 100; i++)
		cout << a[i] << " ";
	cout << (total < 100 ? "" : "...") << endl;
	cout << (is_sorted(a, total) ? "sorted" : "not-sorted") << endl;
}
