#include <iostream>
#include <queue>
#include <algorithm>

#define MAXN	105
#define MAXM	10005

using namespace std;

struct edge {
	int u, v, w;
} eds[MAXM];

bool operator<(const edge& a, const edge& b)
{
	return a.w < b.w;
}

int fa[MAXN];

int root(int u)
{
	return u == fa[u] ? u : fa[u] = root(fa[u]);
}

int kruskal(int n, int tot)
{
	for (int i = 0; i < n; i++)
		fa[i] = i;
	sort(eds, eds+tot);

	int count = 0, len = 0;
	for (int i = 0; i < tot; i++) {
		int u = eds[i].u, v = eds[i].v;
		if (root(u) == root(v))
			continue;

		fa[root(u)] = root(v);
		count++;
		len += eds[i].w;
		if (count+1 == n)
			break;
	}
	return len;
}

// https://www.luogu.com.cn/problem/P1546
// echo "4 0 4 9 21 4 0 8 17 9 8 0 16 21 17 16 0" | ./bin/kruskal
int main()
{
	int n;
	cin >> n;

	int tot = 0;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++) {
			int w;
			cin >> w;
			if (i == j)
				continue;
			eds[tot].u = i;
			eds[tot].v = j;
			eds[tot].w = w;
			tot++;
		}

	cout << kruskal(n, tot) << endl;
}
