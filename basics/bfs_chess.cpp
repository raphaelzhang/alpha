#include <iostream>
#include <cstdio>
#include <queue>

#define MAXN	410

using namespace std;

struct node {
	int x, y, step;
};

int next_x[8] = {-2, -1, 1, 2, 2, 1, -1, -2};
int next_y[8] = {1, 2, 2, 1, -1, -2, -2, -1};

int mat[MAXN][MAXN];

void bfs(int n, int m, int x1, int y1)
{
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			mat[i][j] = -1;

	queue<node> q;
	q.push(node{x1, y1, 0});

	while (!q.empty()) {
		node nd = q.front();
		q.pop();

		if (mat[nd.x][nd.y] != -1)
			continue;

		mat[nd.x][nd.y] = nd.step;

		for (int i = 0; i < 8; i++) {
			int x = nd.x + next_x[i];
			int y = nd.y + next_y[i];
			if (x < 1 || y < 1 || x > n || y > m || mat[x][y] != -1)
				continue;

			q.push(node{x, y, nd.step+1});
		}
	}
}

//https://www.luogu.com.cn/problem/P1443
//echo "3 3 1 1" | ./bin/bfs
int main()
{
	int n, m, x, y;
	cin >> n >> m >> x >> y;
	bfs(n, m, x, y);

	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			printf(j == m ? "%d\n" : "%-5d", mat[i][j]);
}
