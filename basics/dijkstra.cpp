#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

// https://www.luogu.com.cn/problem/P1339
// cat data/p1339.in | ./bin/dijkstra
// result should be 7

#define MAXV	2600
#define MAXE	12500

int total_edges;

struct edge {
	int next;
	int node;
	int val;
} edges[MAXE];

int first[MAXV], visited[MAXV], dists[MAXV];

int add_edge(int u, int v, int w)
{
	edges[++total_edges].next = first[u];
	first[u] = total_edges;
	edges[total_edges].val = w;
	edges[total_edges].node = v;
	return total_edges;
}

void dijkstra(int s, int e)
{
	memset(dists, 0x3f, sizeof(dists));
	dists[s] = 0;

	priority_queue<pair<int, int> > q;
	q.push(make_pair(-dists[s], s));

	while (!q.empty()) {
		int u = q.top().second;
		q.pop();
		if (visited[u])
			continue;

		visited[u] = 1;
		for (int ed = first[u]; ed; ed = edges[ed].next) {
			if (visited[edges[ed].node])
				continue;

			bool changed = false;
			if (dists[edges[ed].node] > dists[u] + edges[ed].val) {
				dists[edges[ed].node] = dists[u] + edges[ed].val;
				changed = true;
			}
			if (changed)
				q.push(make_pair(-dists[edges[ed].node], edges[ed].node));
		}
	}
}

int main()
{
	int towns, routes, s, e;
	cin >> towns >> routes >> s >> e;

	for (int i = 0; i < routes; i++) {
		int u, v, w;
		cin >> u >> v >> w;
		add_edge(u, v, w);
		add_edge(v, u, w);
	}

	dijkstra(s, e);
	cout << dists[e] << endl;
}
