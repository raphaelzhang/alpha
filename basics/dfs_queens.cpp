#include <iostream>

using namespace std;

int mat[16][16];

void flag(int r, int c, int dim, int delta)
{
	for (int i = 0; i < dim; i++) {
		mat[i][c] += delta;
		mat[r][i] += delta;
		// the following codes will include cross-bounds positions so are wrong
		// mat[(r+i) % dim][(c+i) % dim] += delta;
		// mat[(r+i) % dim][(c-i+dim) % dim] += delta;

		int t = c - r + i;
		if (t >= 0 && t < dim)
			mat[i][t] += delta;

		t = c + r - i;
		if (t >= 0 && t < dim)
			mat[i][t] += delta;
	}
}

void dfs(int col, int dim, int result[], int& total)
{
	if (col == dim) {
		total++;
		// show the first 3 only
		if (total <= 3) {
			for (int i = 0; i < col; i++)
				cout << 1+result[i] << " ";
			cout << endl;
		}
		return;
	}

	for (int r = 0; r < dim; r++) {
		if (mat[r][col] != 0)
			continue;

		flag(r, col, dim, 1);
		result[col] = r;
		dfs(col+1, dim, result, total);
		flag(r, col, dim, -1);
	}
}

//https://www.luogu.com.cn/problem/P1219
//echo 6 | ./bin/dfs
int main()
{
	int n;
	cin >> n;

	int total = 0;
	int result[16] = {0};
	dfs(0, n, result, total);
	cout << total << endl;
}
