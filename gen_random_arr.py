#!/usr/bin/env python

import sys
import random

n = 20
upper = 100
print_n = False

if len(sys.argv) >= 2:
	n = int(sys.argv[1])

if len(sys.argv) >= 3:
	upper = int(sys.argv[2])

if len(sys.argv) >= 4:
	print_n = True

if print_n:
	print n,

for _ in xrange(n):
	print random.randint(0, upper),
print
